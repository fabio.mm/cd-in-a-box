#!/bin/bash

export LANG="en_US.UTF-8"
export LANGUAGE="en_US:en"
export LC_ALL="en_US.UTF-8"
export TIMEZONE="Europe/Rome"

echo LC_ALL="${LC_ALL}" >> /etc/environment
echo LANGUAGE="${LANGUAGE}" >> /etc/environment
locale-gen ${LANG}
timedatectl --adjust-system-clock set-timezone ${TIMEZONE} || echo ${TIMEZONE} > /etc/timezone

dpkg-reconfigure -f noninteractive tzdata

apt-get install -y \
	wget curl git unzip vim supervisor \
	netcat socat tcpdump tcpflow dnsutils authbind \
	ubuntu-keyring language-pack-en

sudo adduser vagrant vboxsf
