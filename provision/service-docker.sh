#!/bin/bash

# pre-requirements
apt-get update
apt-get install -y apt-transport-https ca-certificates curl software-properties-common

# docker-ce
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable"
apt-get update

apt-cache policy docker-ce
apt-get install -y docker-ce

# docker-compose
curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
docker-compose --version

# docker-check
systemctl status docker | tee
docker info
docker version

usermod -aG docker vagrant
su vagrant -c 'docker run hello-world'
