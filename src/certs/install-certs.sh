#!/bin/bash

set -e

# registry.ws.so
mkdir -p /etc/docker/certs.d/registry.ws.so
cp registry.crt /etc/docker/certs.d/registry.ws.so/ca.crt
ls -lah /etc/docker/certs.d/ /etc/docker/certs.d/registry.ws.so/

# done
echo "Installed. Please run: service docker restart"
