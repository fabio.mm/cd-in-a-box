# cd-in-a-box

PROJECT = cd-in-a-box

.PHONY: all

clean :
	@echo "[$@]"

configure :
	@echo "[$@]"

up : 
	@echo "[$@]"
	vagrant up

halt :
	@echo "[$@]"
	vagrant halt

destroy :
	@echo "[$@]"
	vagrant destroy
