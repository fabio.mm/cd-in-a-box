
# cd-in-a-box

A sample dev environment to experiment DevOps tasks.

## Installation

- VirtualBox: https://www.virtualbox.org/wiki/Downloads
- vagrant: https://www.vagrantup.com/docs/installation/

## Bootstrapping

Create the Virtual Machine

- `vagrant up`

Once finished, login via SSH using:

- `vagrant ssh`

Access project file moving into:

- `cd cd-in-a-box/`

### Application data

All persistent data is stored in a `DATA_DIR` which is `/srv/data/`.
If this is the first run, initialize it:

- `make initialize-data-dir`

## Usage

The cluster of services is managed with `docker-compose`. Some useful commands are scripted in a `Makefile`:

### Managing services

At the first run you need to build base images:

- `make build`: build base containers

Then you can state of the cluster:

- `make up`: bring services up (it runs `build` first by default)
- `make down`: bring services down
- `make kill`: bring services down and remove them

To check the state of running services:

- `docker ps`

### Fixtures

If you want load project samples and fixtures use:

- `make load-schemas`
- `make load-fixtures`

## Services

In order to resolve the custom `ws.so` urls, add the contents of `provision/hosts` to your `/etc/hosts`.

### Gogs (git repositories)

- Interface: http://git.ws.so/

The admin username is `git` with password `git`.

### Jenkins (Automation server)

- Interface: http://jenkins.ws.so/
- `make build-jenkins-images`: rebuilds executor images

### Docker Registry

- `https://registry.ws.so/`: is the root url
- https://registry.ws.so/v2/_catalog can be use to check stored images

A self-signed certificate is in use and stored in `certs/registry.crt`.
Copy it to your machine in order to avoid SSL warnings.

### Load Balancer (nginx)

A dummy test page could be accessed at http://load-balancer.ws.so/

### MySQL

An instance of MySQL is running as support to other applications (Gogs).

- User: `root`
- Password: `root`
- Hostname: `mysql.ws.so`
- Port: `43306`