# check
ls -l
php -v

# lint
for i in `ls -1 src/*.php`; do php -l $i; done;
#php -l src/App.php 
#php -l src/index.php

# library
composer install

# tests
vendor/bin/phpunit --log-junit='results.xml' test/ExampleSuite.php

# package
mkdir build/
echo $GIT_COMMIT > version
tar cvzf build/hello-world-web_$GIT_COMMIT.tar.gz server.sh version src/
